import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Navigation, Footer, Login, Chat, Planning } from "./components";
function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={() => <Login />} />
          <Route path="/chat" exact component={() => <Chat />} />
          <Route path="/planning" exact component={() => <Planning />} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
