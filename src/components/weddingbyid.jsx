import axios from "axios";
import { useState } from "react";
 
export default function WeddingIdentify(){
 const [weddings,setWeddings] = useState([])
 
 async function getWeddingById(event){
 const response = await axios.get('http://35.223.104.39:3002/weddings')
 const weddingData = response.data;
 setWeddings(weddingData)
 }
 
 const tableRows = weddings.map(w => <tr> <td>{w.weddingId}</td><td>{w.weddingName}</td></tr>)
 return(<div>
     <button onClick={getWeddingById}>Get Wedding by ID</button>
        <table><thead><th>Wedding ID</th><th>Name</th></thead>{tableRows}</table>
 </div>)
 
}