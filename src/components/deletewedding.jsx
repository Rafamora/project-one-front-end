import axios from "axios";
import { useState } from "react";
import { useRef } from "react";
 
export default function WeddingDelete(){
 const [wedding,setWedding] = useState({})
 const idInput = useRef(null)
 
 async function deleteWedding(event){
 const weddingId = idInput.current.value;
 console.log(weddingId);
 const response = await axios.delete(`http://35.223.104.39:3002/weddings/${weddingId}`)
 alert("You deleted a wedding by ID. Please reach out to our team if this was an error.")
 const weddingData = response.data;
 console.log(weddingData)
 setWedding(weddingData)
 }
 
 return(<div>
 <button onClick={deleteWedding}>Delete Wedding by ID</button>
 <input ref={idInput}/>
 
 </div>)
}