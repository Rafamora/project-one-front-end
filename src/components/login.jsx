import React from "react";

function Login() {
  return (
    <div className="login">
      <div class="container">
        <div class="row align-items-center my-10">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://r1.ilikewallpaper.net/iphone-wallpapers/download/74215/Her-Day-iphone-wallpaper-ilikewallpaper_com.jpg" width="600" height ="600"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-light">Login</h1>
            <hr></hr>
            <p>
            <b>Welcome to R&H Wedding Planning.</b> With national recognition and awards, R&H Wedding Planning leads the way in producing highly personal and unforgettable <b>celebrations.</b>
            </p>
            <hr></hr>
            <form>
              <label>
                  <p>Username</p>
                  <input type="text" />
              </label>
              <label>
                  <p>Password</p>
                  <input type="password" />
              </label>
              <div>
              <button type="submit">Submit</button>
              </div>
              <hr></hr>
            </form>
            <input type="file"/>

          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
