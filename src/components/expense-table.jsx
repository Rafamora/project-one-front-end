import axios from "axios";
import { useState } from "react";
 
export default function ExpenseTable(){
 const [expenses,setExpenses] = useState([])
 
 async function getAllExpenses(event){
 const response = await axios.get('http://35.223.104.39:3002/expenses')
 const expenseData = response.data;
 setExpenses(expenseData)
 }
 
 const tableRows = expenses.map(e => <tr> <td>{e.expenseId}</td><td>{e.expenseReason}</td>{e.expenseAmount}<td></td></tr>)
 return(<div>
     <button onClick={getAllExpenses}>Get All Expenses</button>
        <table><thead><th>Expense ID</th><th>Reason</th><th>Amount</th></thead>
        {tableRows}
        </table>
 </div>)
 
}