import axios from "axios";
import { useState } from "react";
import { useRef } from "react";
 
export default function ExpenseDelete(){
 const [expense,setExpense] = useState({})
 const idInput = useRef(null)
 
 async function deleteExpense(event){
 const expenseId = idInput.current.value;
 console.log(expenseId);
 const response = await axios.delete(`http://35.223.104.39:3002/expenses/${expenseId}`)
 alert("You deleted an expense. If this was an error, please reach out to our team.")
 const expenseData = response.data;
 console.log(expenseData)
 setExpense(expenseData)
 }
 
 return(<div>
 <button onClick={deleteExpense}>Delete Expense by ID</button>
 <input ref={idInput}/>
 
 </div>)
}