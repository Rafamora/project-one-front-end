import { useRef } from "react"

export default function MoreInfoForm(){

    // react hooks are special methods provided by react
    // "hook" refers to how these latch onto your component adding functionalties
    // componenets have a lifecycle of being created, rendered, rerenderd, 
    // "hooks" insert themselves into that lifecycle

    // useRef creates a SINGLE object that can mutate that holds a value
    const emailInput = useRef(null); // easiest way to get values from an input/textfield

    function registerEmail(event){
        const value = emailInput.current.value;
        if(!value.includes("@")){
            alert("Not a valid email")
        }else{
            alert(`An email has been sent to ${value} with more information`)
        }
    
    }


    return(<div>

        <h2 class="font-weight-light">Request more information</h2>
        <p class="font-weight-light">You can request more information by adding your email below.</p>

        <label htmlFor="email">Email Address</label>
        <input name="email" ref={emailInput}/>

        <button onClick={registerEmail}>Email Me</button>

    </div>)
}
