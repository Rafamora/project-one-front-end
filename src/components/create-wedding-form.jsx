import axios from "axios";
import { useRef } from "react"
 
export default function WeddingForm(){
 
 const weddingNameInput = useRef(null);
 const weddingLocationInput = useRef(null);
 const weddingDateInput = useRef(null);
 const weddingBudgetInput = useRef(null);
 
 async function addWedding(){
 
 const wedding = {
    weddingId:0,
    weddingName:weddingNameInput.current.value,
    weddingLocation:weddingLocationInput.current.value,
    weddingDate:weddingDateInput.current.value,
    weddingBudget:Number(weddingBudgetInput.current.value),
 }
 
 const response = await axios.post('http://35.223.104.39:3002/weddings',wedding)
 alert("You created a new wedding!A member of our team will reach out shortly!")
 }
 return(<div>
 
 <h3>Wedding Form</h3>
 
 <input placeholder="Name" ref={weddingNameInput}></input>
 <input placeholder="Location" ref={weddingLocationInput}></input>
 <input placeholder="Date" ref={weddingDateInput}></input>
 <input placeholder="Budget" ref={weddingBudgetInput} type="number"></input>
 <button onClick={addWedding}>Create Wedding</button>
 
 </div>)
}