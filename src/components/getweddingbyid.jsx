import axios from "axios";
import { useState } from "react";
import { useRef } from "react";
 
export default function WeddingDetails(){
 const [wedding,setWedding] = useState({})
 const idInput = useRef(null)
 
 async function getWeddingById(event){
 const weddingId = idInput.current.value;
 console.log(weddingId);
 const response = await axios.get(`http://35.223.104.39:3002/weddings/${weddingId}`)
 const weddingData = response.data;
 console.log(weddingData)
 setWedding(weddingData)
 }
 
 return(<div>
 <button onClick={getWeddingById}>Get Wedding by ID</button>
 <input ref={idInput}/>
 <table><thead><th><h6>Name</h6></th><th><h6>Wedding ID</h6></th></thead></table>
 <h5>{wedding.weddingName} {wedding.weddingId}</h5>

 </div>)
}