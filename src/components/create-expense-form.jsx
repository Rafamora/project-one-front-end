import axios from "axios";
import { useRef } from "react"
 
export default function ExpenseForm(){
 
 const expenseIdInput = useRef(null);
 const expenseReasonInput = useRef(null);
 const expenseAmountInput = useRef(null);
 
 async function addExpense(){
 
 const expense = {
    expenseId:0,
    expenseReason:expenseReasonInput.current.value,
    expenseAmount:Number(expenseAmountInput.current.value),
    }
 
 const response = await axios.post('http://35.223.104.39:3002/expenses',expense)
 alert("You created a new expense!")
 }
 return(<div>
 
 <h3>Expense Form</h3>
 
 <input placeholder="Expense Reason" ref={expenseReasonInput}></input>
 <input placeholder="Expense Amount" ref={expenseAmountInput} type="number"></input>
 <button onClick={addExpense}>Create Expense</button>
 
 </div>)
}
