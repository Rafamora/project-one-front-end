import axios from "axios";
import { useState } from "react";
 
export default function WeddingTable(){
 const [weddings,setWeddings] = useState([])
 
 
 async function getAllWeddings(event){
 const response = await axios.get('http://35.223.104.39:3002/weddings')
 const weddingData = response.data;
 setWeddings(weddingData)
 }
 
 const tableRows = weddings.map(w => <tr> <td>{w.weddingId}</td><td>{w.weddingName}</td><td>{w.weddingLocation}</td><td>{w.weddingDate}</td></tr>)
 return(<div>
     <button onClick={getAllWeddings}>Get All Weddings</button>
        <table><thead><th>Wedding ID</th><th>Name</th><th>Location</th><th>Date</th></thead>{tableRows}</table>
 </div>)
 
}
